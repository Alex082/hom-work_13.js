/*

1 Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

    setInterval() запускается не один раз а несколько через то время что мы укажем

2 Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

    ну она будет будет вызвана как можно быстрее но не сразу., 
    нет она не сработает моментально потому что сначала сработает а часть кода которая стоит в очереди

3 Чому важливо не забувати викликати функцію clearInterval(),
 коли раніше створений цикл запуску вам вже не потрібен?

    потому что если этого не делать интервал будет продолжать работать чем будет грузить роботу кода,
    что может закончится не хорошо

*/

// получаем элементы с DOM
const img = document.querySelectorAll(".image-to-show");
const stopBtn = document.getElementById("stop");
const resumBtn = document.getElementById("resum");

// делаем функцыю для показа фото по индекчу
function indexImg(index) {
  for (let i = 0; i < img.length; i++) {
    img[i].style.display = "none";
  }
  img[index].style.display = "block";
}
let yourIndex = 0;

// делаем функцыю для интервала
function startInterval() {
  interval = setInterval(function () {
    yourIndex++;

    if (yourIndex >= img.length) {
      yourIndex = 0;
    }

    indexImg(yourIndex);
  }, 3000);
}

// при загрузке страницы вызываем интервал
startInterval();

// ставим клики на кнопки
stopBtn.addEventListener("click", function () {
  clearInterval(interval);
});

resumBtn.addEventListener("click", function () {
  startInterval();
});
